//
//  ReuseIdentifying.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation
import UIKit

protocol ReuseIdentifying {
    static var reuseIdentifier: String { get }
}
extension ReuseIdentifying {
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
}
extension UITableViewCell: ReuseIdentifying {}
extension UIViewController: ReuseIdentifying {}
