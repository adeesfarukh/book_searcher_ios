//
//  UIControls+Bindable.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation
import UIKit

extension UILabel : Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return self.text
    }
    
    public func updateValue(with value: String) {
        self.text = value
    }
}

extension UITextField : Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return self.text
    }
    
    public func updateValue(with value: String) {
        self.text = value
    }
}

extension UIButton : Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return self.titleLabel?.text
    }
    
    public func updateValue(with value: String) {
        self.setTitle(value, for: .normal)
    }
}

extension UIImageView : Bindable {
    public typealias BindingType = UIImage
    
    public func observingValue() -> UIImage? {
        return self.image
    }
    
    public func updateValue(with value: UIImage) {
        self.image = value
    }
}

extension UITextView : Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return self.text
    }
    
    public func updateValue(with value: String) {
        self.text = value
    }
}
