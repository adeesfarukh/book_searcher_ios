//
//  ClosureSleeve.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import UIKit

class ClosureSleeve: NSObject {
  let closure: ()->()

  init (_ closure: @escaping ()->()) {
    self.closure = closure
  }

  @objc func invoke () {
    closure()
  }
}

extension UIControl {
  func addAction(for controlEvents: UIControl.Event = .touchUpInside, _ closure: @escaping ()->()) {
    let sleeve = ClosureSleeve(closure)
    addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
  }
}

