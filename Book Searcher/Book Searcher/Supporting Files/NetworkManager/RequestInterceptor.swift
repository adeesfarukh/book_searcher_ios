//
//  RequestInterceptor.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//


import Foundation

public protocol RequestInterceptor {
  func addHeaders(request: inout URLRequest)
}
