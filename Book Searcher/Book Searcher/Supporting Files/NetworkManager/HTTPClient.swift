//
//  HTTPClient.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//


import Foundation

public typealias HTTPClientCompletion<T: Endpoint> = (Result<T.ResponseType, Error>) -> Void

public class HTTPClient {
  
  public static let shared = HTTPClient()
  
  public var interceptor: RequestInterceptor?
  
  public func performRequest<T: Endpoint>(endpoint: T, completion: @escaping HTTPClientCompletion<T>) {
    
    let session = URLSession.shared
    
    do {
      
      let request = try self.buildRequest(from: endpoint)
      NetworkLogger.log(request: request)
      
      let task = session.dataTask(with: request) { (data, response, error) in
        
        if let error = error {
          completion(.failure(error))
          return
        }
        
        guard let response = response as? HTTPURLResponse else {
          completion(.failure(NetworkResponse.noResponse))
          return
        }
        NetworkLogger.log(response: response)
        
        self.handleResponse(endpoint: endpoint,
                            data: data,
                            response: response,
                            error: error,
                            completion: completion)
      }
      
      task.resume()
    }
      
    catch let error {
      completion(.failure(error))
    }
  }
  
  private func handleResponse<T: Endpoint>(endpoint:T, data: Data?, response: HTTPURLResponse, error: Error?, completion: @escaping HTTPClientCompletion<T>) {
    
    let result = self.handleNetworkResponse(response)
    
    switch result {
    case .success:
      guard let data = data else {
        completion(.failure(NetworkResponse.noData))
        return
      }
      mapJsonDataToModel(endpoint: endpoint, data: data, response: response, completion: completion)
      
    case .failure(let error):
      guard let data = data else {
        completion(.failure(error))
        return
      }
      // check failure message if any
      do {
        let result = try JSONDecoder().decode(ErrorResult.self, from: data)
        let error = NSError(domain: "MeisterNetwork",
                            code: response.statusCode,
                            userInfo: [NSLocalizedFailureReasonErrorKey : result.error?.description ?? "Unknown"])
        completion(.failure(error))
      }
      catch {
        let message = String(data: data, encoding: .utf8)
        completion(.failure(NetworkResponse.custom(message: message ?? "Unknown")))
      }
    }
  }
  
  private func mapJsonDataToModel<T: Endpoint>(endpoint: T, data: Data, response: HTTPURLResponse, completion: @escaping HTTPClientCompletion<T>) {
    NetworkLogger.log(data: data)
    do {
      let result = try JSONDecoder().decode(T.ResponseType.self, from: data)
      completion(.success(result))
    }
    catch let error {
      completion(.failure(error))
    }
  }
  
}

fileprivate extension HTTPClient {
  
  func buildRequest<T: Endpoint>(from endpoint: T) throws -> URLRequest {
    
    var request = URLRequest(url: endpoint.baseURL.appendingPathComponent(endpoint.path),
                             cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                             timeoutInterval: 10.0)
    
    request.httpMethod = endpoint.httpMethod.rawValue
    self.addHeaders(endpoint.httpHeaders, request: &request)
    
    
    do {
      switch endpoint.encoding {
      case .urlEncoding:
        try self.configureParameters(bodyParameters: endpoint.bodyParamters?.paramForHTTPClient,
                                     bodyEncoding: .urlEncoding,
                                     urlParameters: endpoint.urlParamters?.paramForHTTPClient,
                                     request: &request)
      case .jsonEncoding:
        try self.configureParameters(bodyParameters: endpoint.bodyParamters?.paramForHTTPClient,
                                     bodyEncoding: .jsonEncoding,
                                     urlParameters: endpoint.urlParamters?.paramForHTTPClient,
                                     request: &request)
      case .urlAndJsonEncoding:
        try self.configureParameters(bodyParameters: endpoint.bodyParamters?.paramForHTTPClient,
                                     bodyEncoding: .urlAndJsonEncoding,
                                     urlParameters: endpoint.urlParamters?.paramForHTTPClient,
                                     request: &request)
      }
      return request
    }
      
    catch {
      throw error
    }
  }
  
  func configureParameters(bodyParameters: Parameters?,
                           bodyEncoding: ParameterEncoding,
                           urlParameters: Parameters?,
                           request: inout URLRequest) throws {
    do {
      try bodyEncoding.encode(urlRequest: &request,
                              bodyParameters: bodyParameters,
                              urlParameters: urlParameters)
    } catch {
      throw error
    }
  }
  
  func addHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
    guard let headers = additionalHeaders else { return }
    for (key, value) in headers {
      request.setValue(value, forHTTPHeaderField: key)
    }
    interceptor?.addHeaders(request: &request)
  }
  
  func handleNetworkResponse(_ response: HTTPURLResponse) -> ResultInternal<Error> {
    var ownResponse = NetworkResponse.failed
    switch response.statusCode {
    case 200...299: return .success
    case 400...499: ownResponse = NetworkResponse.authenticationError
    case 500...599: ownResponse = NetworkResponse.serverDown
    case 600: ownResponse = NetworkResponse.outdated
    default: ownResponse = NetworkResponse.failed
    }
    
    let error = NSError(domain: "MeisterNetworkModule",
                        code: response.statusCode,
                        userInfo: [NSLocalizedFailureReasonErrorKey : ownResponse.localizedDescription])
    return ResultInternal.failure(error)
  }
  
}

fileprivate enum ResultInternal<Error>{
  case success
  case failure(Error)
}

fileprivate enum NetworkResponse: Error {
  case success
  case authenticationError
  case badRequest
  case outdated
  case failed
  case noData
  case noResponse
  case unableToDecode
  case serverDown
  case custom(message: String)
}

extension NetworkResponse: LocalizedError {
  
  var errorDescription: String? {
    switch self {
    case .success:
      return "success"
    case .authenticationError:
      return "You need to be authenticated first."
    case .badRequest:
      return  "Bad request"
    case .outdated:
      return  "The url you requested is outdated."
    case .failed:
      return "Network request failed."
    case .noData:
      return "Response returned with no data to decode."
    case .noResponse:
      return "No response."
    case .unableToDecode:
      return  "We could not decode the response."
    case .serverDown:
      return "Something went wrong. Please try again later"
    case .custom(let message):
      return message
    }
    
  }
}


struct ErrorResult : Decodable {
  let error : OwnError?
  enum CodingKeys: String, CodingKey {
    case error = "error"
  }
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    error = try values.decodeIfPresent(OwnError.self, forKey: .error)
  }
}

struct OwnError : Decodable {
  let description : String?
  enum CodingKeys: String, CodingKey {
    case description = "description"
  }
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    description = try values.decodeIfPresent(String.self, forKey: .description)
  }
}


