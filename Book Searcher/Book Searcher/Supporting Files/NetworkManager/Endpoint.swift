//
//  Endpoint.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation

public enum HTTPMethod: String {
  case get
  case post
  case put
  case delete
}

public typealias HTTPHeaders = [String: String]


public protocol ParametersConstraints {
  var paramForHTTPClient: [String:Any] {  get }
}

public extension ParametersConstraints {
  var paramForHTTPClient: [String: Any] { dictionary }
  var dictionary: [String:Any] {
    let mirror = Mirror(reflecting: self)
    
    let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
      var resultValue = value
      guard let label = label else { return nil }
      if case Optional<Any>.none = resultValue {  return nil  }
      // check to ignore key if it has empty value
      if let string = resultValue as? String, string.isEmpty {  return nil  }
      // check to assign key (1 or 0) string value in case of bool
      if let bool = resultValue as? Bool {  resultValue = bool ? "1" : "0"  }
      return (label, resultValue)
    }).compactMap { $0 })
    return dict
  }
}

public protocol Endpoint {
  associatedtype ResponseType: Codable
  associatedtype Parameters: ParametersConstraints
  
  var baseURL: URL { get }
  var path: String {  get }
  var httpMethod: HTTPMethod {  get }
  var httpHeaders: HTTPHeaders {  get }
  var bodyParamters: Parameters? { get }
  var urlParamters: Parameters? { get }
  var encoding: ParameterEncoding { get }
}

public protocol EmapsEndpoint: Endpoint {}

extension Endpoint {
  
  var environmentBaseURL : String {
    return "https://www.googleapis.com/books/v1"
  }
  
  var baseURL: URL {
    guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
    return url
  }
  
  var encoding: ParameterEncoding { .jsonEncoding }

  public var headers: HTTPHeaders { defaultHeaders  }
  
  public var defaultHeaders: [String: String] {
    let headers = [
      "Accept" : "application/json",
      "Content-Type" : "application/json; charset=utf-8",
    ]
    return headers
  }

  
  public func call(completion: @escaping (Result<ResponseType, Error>) -> Void) {
    HTTPClient.shared.performRequest(endpoint: self, completion: completion)
  }

}
