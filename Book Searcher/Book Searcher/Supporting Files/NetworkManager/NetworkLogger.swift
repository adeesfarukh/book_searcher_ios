//
//  NetworkLogger.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation

class NetworkLogger {
  static func log(request: URLRequest) {
    
    print("\n - - - - - - - - - - OUTGOING - - - - - - - - - - \n")
    defer { print("\n - - - - - - - - - -  END - - - - - - - - - - \n") }
    
    let urlAsString = request.url?.absoluteString ?? ""
    let urlComponents = NSURLComponents(string: urlAsString)
    
    let method = request.httpMethod != nil ? "\(request.httpMethod ?? "")" : ""
    let path = "\(urlComponents?.path ?? "")"
    let query = "\(urlComponents?.query ?? "")"
    let host = "\(urlComponents?.host ?? "")"
    
    var logOutput = """
                        \(urlAsString) \n\n
                        \(method) \(path)?\(query) HTTP/1.1 \n
                        HOST: \(host)\n
                        """
    for (key,value) in request.allHTTPHeaderFields ?? [:] {
      logOutput += "\(key): \(value) \n"
    }
    if let body = request.httpBody {
      logOutput += "\n \(NSString(data: body, encoding: String.Encoding.utf8.rawValue) ?? "")"
    }
    
    print(logOutput)
  }
  
  static func log(response: URLResponse) {
    print(response)
  }
  
  static func log(data: Data) {
    do {
      let jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
      print(jsonData)
    }
    catch let error {
      print(error.localizedDescription)
    }
  }
  
}
