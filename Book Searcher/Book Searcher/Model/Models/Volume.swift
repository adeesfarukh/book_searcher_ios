//
//  Volume.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//


import Foundation
struct Volume : Codable {
	let kind : String?
	let totalItems : Int?
    var items : [Items]?

	enum CodingKeys: String, CodingKey {

		case kind = "kind"
		case totalItems = "totalItems"
		case items = "items"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		kind = try values.decodeIfPresent(String.self, forKey: .kind)
		totalItems = try values.decodeIfPresent(Int.self, forKey: .totalItems)
		items = try values.decodeIfPresent([Items].self, forKey: .items)
	}

}
