//
//  SearchAPI.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation

struct SearchAPI: Endpoint {
  var path: String = "/volumes"
  var httpMethod: HTTPMethod = .get
  var httpHeaders: HTTPHeaders { defaultHeaders }
  var bodyParamters: Parameters? = nil
  var urlParamters: Parameters?
  var encoding: ParameterEncoding { .urlEncoding }
  struct Parameters : ParametersConstraints {
    var q : String
  }
  typealias ResponseType = Volume
}
