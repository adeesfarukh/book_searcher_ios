//
//  SearchViewModel.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import Foundation
import ANLoader

protocol SearchViewModelDelegates: AnyObject {
    func showError(with error: Error?)
    func refreshUI()
}

class SearchViewModel: NSObject {
    // MARK: - Variables
    weak var delegate: SearchViewModelDelegates?
    var volume: Observable<Volume> = Observable()
    var volumeInfo: Observable<VolumeInfo> = Observable()
    // MARK: - Init
    override init() {
        super.init()
    }
    
    func filter(text:String? = nil) {
        ANLoader.showLoading()
        search(text) { result in
            ANLoader.hide()
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.volume.value = response
                    self.delegate?.refreshUI()
                }
            case .failure(let error):
                self.delegate?.showError(with: error)
            }
        }
    }
}

extension SearchViewModel {
    typealias SearchCompletionHandler = (Result<SearchAPI.ResponseType, Error>) -> Void
    private func search(_ query: String? = nil,
                        completion: @escaping SearchCompletionHandler) {
        let params = SearchAPI.Parameters(q: query ?? "")
        let request = SearchAPI(urlParamters: params)
        request.call(completion: completion)
    }
}
