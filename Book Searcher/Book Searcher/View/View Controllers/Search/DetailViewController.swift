//
//  DetailViewController.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import UIKit

class DetailViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet var bookImageView: UIImageView!
    @IBOutlet var bookTitleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var descriptionTV: UITextView!
    
    // MARK: - Variables
    var viewModel: SearchViewModel?
    
    // MARK: - Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
    }
    private func populateData() {
        let volume = viewModel?.volumeInfo.value
        bookTitleLabel.text = volume?.title
        descriptionTV.text = volume?.description
        authorLabel.text = volume?.authors?.joined(separator: ", ") ?? ""
        if let imageLinks = volume?.imageLinks, let url = URL(string: imageLinks.thumbnail ?? "") {
            bookImageView.sd_setImage(with: url)
        }
    }
}
