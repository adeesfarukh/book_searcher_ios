//
//  SearchTableViewCell.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import UIKit
import SDWebImage

class SearchTableViewCell: UITableViewCell {

    @IBOutlet var bookTitleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var bookImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func populateData(volume: VolumeInfo?) {
        bookTitleLabel.text = volume?.title
        authorLabel.text = volume?.authors?.joined(separator: ", ") ?? ""
        if let imageLinks = volume?.imageLinks, let url = URL(string: imageLinks.thumbnail ?? "") {
            bookImageView.sd_setImage(with: url)
        }
    }
}
