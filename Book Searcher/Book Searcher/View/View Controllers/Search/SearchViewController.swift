//
//  SearchViewController.swift
//  Book Searcher
//
//  Created by Adees Farakh on 17.02.22.
//

import UIKit

class SearchViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    // MARK: - Variables
    lazy var viewModel: SearchViewModel = {
        var viewModel = SearchViewModel()
        viewModel.delegate = self
        return viewModel
    }()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailViewController {
            vc.viewModel = viewModel
        }
    }
}

// MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.volume.value?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.reuseIdentifier, for: indexPath) as? SearchTableViewCell else {
            preconditionFailure()
        }
        let volumeInfo = viewModel.volume.value?.items?[indexPath.item].volumeInfo
        cell.populateData(volume: volumeInfo)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.volumeInfo.value = viewModel.volume.value?.items?[indexPath.item].volumeInfo
        performSegue(withIdentifier: DetailViewController.reuseIdentifier, sender: self)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

// MARK: - SearchViewModelDelegates
extension SearchViewController: SearchViewModelDelegates {
    func showError(with error: Error?) {
        let error = error as NSError?
        if error?.code == 400 {
            DispatchQueue.main.async {
                self.viewModel.volume.value?.items?.removeAll()
                self.tableView.reloadData()
            }
        }
    }
    
    func refreshUI() {
        tableView.reloadData()
    }
}

// MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filter(text: searchText)
    }
}
